package infra

import java.time.LocalDateTime

interface DateSupplier {
    fun now(): LocalDateTime

}

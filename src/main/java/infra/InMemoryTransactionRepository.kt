package infra

import domain.Amount
import domain.Deposit
import domain.Transaction
import domain.TransactionRepository

class InMemoryTransactionRepository: TransactionRepository {
    val transactions: MutableList<Transaction> = mutableListOf()
    override fun save(transaction: Transaction) {
        transactions.add(transaction)
    }

    override fun getAllTransactions(): List<Transaction> {
        return transactions
    }

    override fun getLastBalance(): Amount {
        return if (transactions.isEmpty()){
              Amount("0")
        } else{
            transactions.last().balance
        }
    }
}
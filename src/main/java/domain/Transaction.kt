package domain

import java.time.LocalDateTime

interface Transaction{
    val amount: Amount
    val date: LocalDateTime
    val balance: Amount
}

package domain

import java.time.LocalDateTime

data class Withdrawal(override val amount: Amount, override val date: LocalDateTime, override val balance: Amount): Transaction
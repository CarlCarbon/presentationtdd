package domain

import java.time.LocalDateTime

data class Deposit(override val amount: Amount, override val date: LocalDateTime, override val balance: Amount): Transaction

package domain

interface TransactionRepository {
    fun save(transaction: Transaction)
    fun getAllTransactions(): List<Transaction>
    fun getLastBalance(): Amount
}

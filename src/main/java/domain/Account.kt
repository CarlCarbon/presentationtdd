package domain

import infra.DateSupplier

class Account(val transactionRepository: TransactionRepository, val dateSupplier: DateSupplier) {
    fun depositMoney(amount: Amount) {
        val lastBalance = transactionRepository.getLastBalance()
        val newBalance: Amount = lastBalance.plus(amount)
        transactionRepository.save(Deposit(amount, dateSupplier.now(), newBalance))
    }

    fun withdrawMoney(amount: Amount) {
        try {
            val lastBalance = transactionRepository.getLastBalance()
            val newBalance: Amount = lastBalance.minus(amount)
            transactionRepository.save(Withdrawal(amount, dateSupplier.now(), newBalance))
        }catch (e: InvalidAmount){
            throw InvalidTransaction("The balance can not be negative")
        }
    }


    fun printStatement(statementPrinter: StatementPrinter) {
        statementPrinter.printStatement(transactionRepository.getAllTransactions())
    }

}

class InvalidTransaction(message: String): Throwable(message)

package domain

import java.math.BigDecimal

class Amount {
    internal var value: BigDecimal = BigDecimal("0")
    constructor( value: String){
        try {
            val bigDecimal = BigDecimal(value)
            if(bigDecimal.compareTo(BigDecimal.ZERO) < 0){
                throw InvalidAmount("Should be positif: $value")
            }
            this.value = bigDecimal
        }catch (e: NumberFormatException){
            throw InvalidAmount("Should be a number: $value")
        }
    }
    fun minus(amount: Amount): Amount {
        return Amount(value.minus(amount.value).toPlainString())
    }

    fun plus(amount: Amount): Amount {
        return Amount(value.add(amount.value).toPlainString())
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Amount

        if (value != other.value) return false

        return true
    }

    override fun hashCode(): Int {
        return value.hashCode()
    }




}

class InvalidAmount(message : String) : Throwable(message)

package infra

import domain.Amount
import domain.Deposit
import domain.Withdrawal
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

internal class InMemoryTransactionRepositoryTest{

    @Test
    internal fun `should save deposit`() {
        val transactionRepository = InMemoryTransactionRepository()

        val deposit = Deposit(Amount("1234"), LocalDateTime.of(1, 2, 3, 1, 1, 1), Amount("1234"))
        transactionRepository.save(deposit)

        assertThat(transactionRepository.getAllTransactions())
            .isEqualTo(listOf(deposit))
    }

    @Test
    internal fun `should get the last balance`() {
        val transactionRepository = InMemoryTransactionRepository()

        transactionRepository.save(Deposit(Amount("1234"), LocalDateTime.of(2020, 2, 3, 1, 1, 1), Amount("1000")))
        transactionRepository.save(Withdrawal(Amount("1234"), LocalDateTime.of(2020, 2, 3, 1, 1, 1), Amount("1000")))
        transactionRepository.save(Deposit(Amount("1234"), LocalDateTime.of(2020, 9, 9, 9, 9, 9), Amount("99999")))

        assertThat(transactionRepository.getLastBalance())
            .isEqualTo(Amount("99999"))
    }

    @Test
    internal fun `should return an amount of zero when there is no transactions`() {
        val transactionRepository = InMemoryTransactionRepository()

        assertThat(transactionRepository.getLastBalance())
            .isEqualTo(Amount("0"))
    }
}
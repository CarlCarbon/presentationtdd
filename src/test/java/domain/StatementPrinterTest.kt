package domain

import infra.DisplayInterface
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

internal class StatementPrinterTest{

    @Test
    internal fun `should print statement when have one deposit`() {
        val displayInterface: DisplayInterface = mockk()
        every {
            displayInterface.print(any())
        }
        val statementPrinter = StatementPrinter(displayInterface)

        statementPrinter.printStatement(
            listOf(
                Deposit(Amount("1000.00"), LocalDateTime.of(2012, 1, 10, 1,1), Amount("1000"))
            )
        )

        verify {
            var expected = """
            date       || credit   || debit    || balance
            10/01/2012 || 1000.00  ||          || 1000.00
            """
            displayInterface.print(expected)
        }
    }
}
package domain

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import kotlin.test.assertFails

internal class AmountTest{
    @Test
    internal fun `amount should always be a number`() {
        assertFails {
            Amount("coucou")
        }
    }

    @Test
    internal fun `amount should always be positive`() {
        assertFails {
            Amount("-13456")
        }
    }

    @Test
    internal fun `should subtract two amount`() {
        assertThat(Amount("10000").minus(Amount("6000")))
            .isEqualTo(Amount("4000"))
    }
}
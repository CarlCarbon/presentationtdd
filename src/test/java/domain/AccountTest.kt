package domain

import infra.DateSupplier
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.*
import kotlin.test.assertFails


internal class AccountTest{
    private val transactionRepository: TransactionRepository = mockk()
    private val dateSupplier: DateSupplier = mockk()
    private val now = LocalDateTime.ofInstant(Instant.ofEpochMilli(1550160535168L), ZoneId.of("UTC"))

    @BeforeEach
    internal fun setUp() {
        every {
            transactionRepository.save(any())
        } returns Unit
        every {
            dateSupplier.now()
        } returns now
    }

    @Test
    internal fun `should be able to deposit money`() {
        val account = Account(transactionRepository, dateSupplier)
        every {
            transactionRepository.getLastBalance()
        } returns Amount("2000")

        account.depositMoney(Amount("500"))

        verify(exactly = 1) {
            transactionRepository.save(Deposit(Amount("500"), now, Amount("2500")))
            dateSupplier.now()
        }
    }

    @Test
    internal fun `should be able to withdraw money`() {
        val account = Account(transactionRepository, dateSupplier)
        every {
            transactionRepository.getLastBalance()
        } returns Amount("2000")

        account.withdrawMoney(Amount("500"))

        verify(exactly = 1) {
            transactionRepository.save(Withdrawal(Amount("500"), now, Amount("1500")))
            dateSupplier.now()
        }
    }

    @Test
    internal fun `should not be able to withdraw money when it make the balance negative`() {
        val account = Account(transactionRepository, dateSupplier)
        every {
            transactionRepository.getLastBalance()
        } returns Amount("1000")

        assertFails("The balance can not be negative")
            {account.withdrawMoney(Amount("9999"))}

        verify(exactly = 0){
            transactionRepository.save(any())
        }
    }

    @Test
    internal fun `should print the account statement`() {
        val account = Account(transactionRepository, dateSupplier)
        val statementPrinter: StatementPrinter = mockk()
        every {
            statementPrinter.printStatement(any())
        } returns Unit
        val transactions = listOf(Deposit(Amount("1234"), now, Amount("1234")))
        every {
            transactionRepository.getAllTransactions()
        } returns transactions


        account.printStatement(statementPrinter)

        verify(exactly = 1){
            statementPrinter.printStatement(transactions)
            transactionRepository.getAllTransactions()
        }
    }
}
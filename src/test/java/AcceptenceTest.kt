import domain.Account
import domain.Amount
import domain.StatementPrinter
import domain.TransactionRepository
import infra.DateSupplier
import infra.DisplayInterface
import infra.InMemoryTransactionRepository
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.time.Clock
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

class AcceptanceTest {

    @Test
    internal fun `bank account acceptance test`() {
        val expected = """
            date       || credit   || debit    || balance
            14/01/2012 ||          || 500.00   || 2500.00
            13/01/2012 || 2000.00  ||          || 3000.00
            10/01/2012 || 1000.00  ||          || 1000.00
            """

        val displayInterface: DisplayInterface = mockk()
        val dateSupplier: DateSupplier = mockk()
        every {
            dateSupplier.now()
        } returns LocalDateTime.ofInstant(Instant.ofEpochMilli(1550160535168L), ZoneId.of("UTC"))

        val statementPrinter = StatementPrinter(displayInterface)
        val transactionRepository = InMemoryTransactionRepository()
        val account = Account(transactionRepository, dateSupplier)

        account.depositMoney(Amount("1000"))
        account.depositMoney(Amount("2000"))
        account.withdrawMoney(Amount("500"))

        account.printStatement(statementPrinter)


        verify(exactly = 1) {
            displayInterface
                .print(expected)
        }
        confirmVerified(displayInterface)
    }

}